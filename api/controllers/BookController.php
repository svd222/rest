<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 29.05.19
 * Time: 13:34
 */
namespace api\controllers;

use Yii;
use common\models\Book;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use common\models\AuthorQuery;

class BookController extends ActiveController
{
    public $modelClass = Book::class;

    public function actions()
    {
        $actions = parent::actions();
        $modelClass = $this->modelClass;
        $actions['index']['prepareDataProvider'] = function ($action) use ($modelClass) {
            $name = Yii::$app->request->get('name', '');
            $authors = Yii::$app->request->get('authors', '');

            /**
             * @var ActiveQuery $query
             */
            $query = $modelClass::find()->where(['status' => 1]);
            if ($authors) {
                $query = $query->innerJoinWith([
                    'authors' => function ($queryAuthors) use ($authors) {
                        $authors = explode(',', $authors);
                        $authors = array_map('trim', $authors);
                        /**
                         * @var AuthorQuery $queryAuthors
                         */
                        $queryAuthors->andWhere(['in', 'fullName', $authors]);
                    }
                ]);
            }

            if ($name) {
                $name = explode(',', $name);
                $name = array_map('trim', $name);
                $query = $query->andWhere(['in', 'name', $name]);
            }

            return new ActiveDataProvider([
                'query' => $query,
            ]);
        };

        $actions['create']['scenario'] = Book::SCENARIO_CREATE_TROUGH_API;
        return $actions;
    }
}