<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 29.05.19
 * Time: 12:27
 */
namespace api\controllers;

use common\models\Author;
use yii\rest\ActiveController;

class AuthorController extends ActiveController
{
    public $modelClass = Author::class;

    public function actions()
    {
        return parent::actions(); // TODO: Change the autogenerated stub
    }
}