<?php

use yii\db\Migration;

/**
 * Class m190529_090346_create_book
 */
class m190529_090346_create_book extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'keywords' => $this->string(),
            'image' => $this->string(),
            'description' => $this->string(),
            'status' => $this->tinyInteger()->notNull()->defaultValue(0)
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%book}}');
    }
}
