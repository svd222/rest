<?php

use yii\db\Migration;

/**
 * Class m190529_090757_create_book_author
 */
class m190529_090757_create_book_author extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book_author}}', [
            'book_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_book_author_book', '{{%book_author}}', 'book_id', '{{%book}}', 'id', 'NO ACTION');
        $this->addForeignKey('FK_book_author_author', '{{%book_author}}', 'author_id', '{{%author}}', 'id', 'NO ACTION');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_book_author_book', '{{%book_author}}');
        $this->dropForeignKey('FK_book_author_author', '{{%book_author}}');

        $this->dropTable('{{%book_author}}');
    }
}
